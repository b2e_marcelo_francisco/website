<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="page" id="top">
 *
 * @package Rhythm
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<Meta name="description" content="Empresa especializada em motor de Concessão de Crédito e Prevenção a Fraude nos Programas de Fidelidade, E-Commerce e na Validação Cadastral." />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62816250-1', 'auto');
  ga('send', 'pageview');

  /*ga('send', {
    'hitType': 'pageview',
    'page': '/home'
  });*/

</script>
</head>
<?php
	$GLOBALS['LINGUAGEM-DEFINIDA'] = 'pb';

	$classesDoCorpo = get_body_class();

	if (array_search('en', $classesDoCorpo) !== false) {
		$GLOBALS['LINGUAGEM-DEFINIDA'] = 'en';
	} elseif (array_search('es', $classesDoCorpo) !== false) {
		$GLOBALS['LINGUAGEM-DEFINIDA'] = 'es';
	}
?>
<body <?php printf('class="%s"', implode(' ', $classesDoCorpo)) ?>>



	
<?php if (ts_get_opt('enable-preloader') == 1): 
	$preloader_custom_image = ts_get_opt_media('preloader-custom-image');
	?>
	<!-- Page Loader -->        
	<div class="page-loader <?php echo ( !empty($preloader_custom_image) ? 'loader-custom-image' : '' ); ?>">
		<?php if (!empty($preloader_custom_image)): ?>
			<div class="loader-image"><img src="<?php echo esc_url($preloader_custom_image); ?>" alt="<?php _e('Loading...', 'rhythm'); ?>" /></div>
		<?php endif; ?>
		<div class="loader"><?php _e('Loading...', 'rhythm'); ?></div>
	</div>
	<!-- End Page Loader -->
<?php endif; ?>

<!-- Page Wrap -->
<div class="page" id="top">
	<?php if (ts_get_opt('enable-under-construction') == 1 && !current_user_can('level_10')): ?>
		<?php get_template_part('templates/header/under-construction'); ?>	
	<?php else: ?>
		<?php get_template_part('templates/preheader/default'); ?>
		<?php ts_get_header_template_part(); ?>
	<?php endif; ?>

<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['account','c22658fcf67c3ac229ee483340a84239']);
_tn.push(['action','track-view']);
(function() {
    document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
    var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
    tss.src = 'https://tracker.tolvnow.com/js/tn.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();</script>

