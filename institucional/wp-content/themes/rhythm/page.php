<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Rhythm
 */

get_header();
ts_get_title_wrapper_template_part();
?>


<div id="pop-overley"></div>


<?php if ($GLOBALS['LINGUAGEM-DEFINIDA'] == 'pb'): ?> 

<div id="captacao-lead-site-5d4d5fba866126f5de51" class="lead-site pb">
	<i class="fa fa-times" id="rdStation-close"></i>
</div>
<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
<script type="text/javascript">
	new RDStationForms('captacao-lead-site-5d4d5fba866126f5de51-html', 'UA-62816250-1').createForm();
</script>

<?php endif; ?>

<?php if ($GLOBALS['LINGUAGEM-DEFINIDA'] == 'en'): ?> 
<div role="main" id="captacao-lead-site-2-5e22485d27d6b02e1a60" class="lead-site en">
	<i class="fa fa-times" id="rdStation-close"></i>
</div>
 <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
 <script type="text/javascript">
	new RDStationForms('captacao-lead-site-2-5e22485d27d6b02e1a60-html', 'UA-62816250-1').createForm();
</script>
<?php endif; ?>

<?php if ($GLOBALS['LINGUAGEM-DEFINIDA'] == 'es'): ?> 
<div role="main" id="ajuda-no-site-676b77fc84c25525bbcf" class="lead-site es">
	<i class="fa fa-times" id="rdStation-close"></i>
</div>
<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
<script type="text/javascript">
	new RDStationForms('ajuda-no-site-676b77fc84c25525bbcf-html', 'UA-62816250-1').createForm();
</script>
<?php endif; ?>

<!-- Page Section -->
<section class="main-section page-section <?php echo sanitize_html_classes(ts_get_post_opt('page-margin-local'));?>">

	<div class="container relative">
		<?php get_template_part('templates/global/page-before-content'); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'templates/content/content','page' ); ?>
			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( ts_get_opt('page-comments-enable') == 1 && (comments_open() || get_comments_number()) ) :
					comments_template();
				endif;
			?>
		<?php endwhile; // end of the loop ?>
		<?php get_template_part('templates/global/page-after-content'); ?>
	</div>
</section>

<!-- End Page Section -->
<?php get_footer();
