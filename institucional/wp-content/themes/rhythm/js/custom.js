function initCustomJS() {

	jQuery('a.benefit-item-link').parent().addClass('bnefit-center-text');
	
	jQuery('.home-banner-btn .local-scroll a.btn-border-w').click(function(e) {
		e.preventDefault();
		abrePopUp();
	});

	jQuery('#rdStation-close').click(function() {
		fechaPopUp();
	});
}

function abrePopUp () {
	if (!jQuery('.lead-site').hasClass('open')) {
		jQuery('.lead-site').css({'top': jQuery(document).scrollTop() + 100 }).addClass('open');
	}
	
	if (!jQuery('#pop-overley').hasClass('open')) {		
		jQuery('#pop-overley').css({'height' : jQuery(document).height()}).addClass('open');
	}
}

function fechaPopUp () {
	if (jQuery('.lead-site').hasClass('open')) {
		jQuery('.lead-site').removeClass('open');
	}
	if (jQuery('#pop-overley').hasClass('open')) {		
		jQuery('#pop-overley').css({'height' : 0}).removeClass('open');;
	}
}

function isModalAberta() {
	var data = new Date();
	var dataAtual = data.getFullYear() + '-' + data.getMonth() + '-' + data.getDay();
	data = localStorage.getItem('tempo'); 

	if (data == null || data == undefined) {
		localStorage.setItem('tempo', dataAtual);
		return false;
	} else if (data != dataAtual) {
		localStorage.clear();
		return false;
	} else {
		return true;
	}
}