<?php
/**
 * Social sites
 */
$labels = array(
		'name'               => __( 'Social Sites', 'rhythm-addons' ),
		'singular_name'      => __( 'Social Site', 'rhythm-addons' ),
		'add_new'            => __( 'Add New','rhythm-addons' ),
		'add_new_item'       => __( 'Add New Social Site','rhythm-addons' ),
		'edit_item'          => __( 'Edit Social Site','rhythm-addons' ),
		'new_item'           => __( 'New Social Site','rhythm-addons' ),
		'all_items'          => __( 'All Social Sites','rhythm-addons' ),
		'view_item'          => __( 'View Social Site','rhythm-addons' ),
		'search_items'       => __( 'Search Social Sites','rhythm-addons' ),
		'not_found'          => __( 'No Social Sites found','rhythm-addons' ),
		'not_found_in_trash' => __( 'No Social Sites found in the Trash','rhythm-addons' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Social Sites', 'rhythm-addons' ),
);

$args = array(
	'labels'        => $labels,
	'public'        => false,
	'show_ui'       => true,
	'menu_position' => 21,
	'supports'      => array( 'title', 'page-attributes' ),
	'has_archive'   => false,
	'rewrite' => array(
		'slug' => 'cpt-social-site'
	)
);
register_post_type( 'social-site', $args );

/**
 * Social sites - replace "enter title here" with "enter url here" when adding new site
 */
function ts_custom_post_social_sites_change_title( $title ){
	$screen = get_current_screen();

	if  ( 'social-site' == $screen->post_type ) {
		$title = __('http:// Enter URL here', 'rhythm-addons');
	}
	return $title;
}
add_filter( 'enter_title_here', 'ts_custom_post_social_sites_change_title' );