<?php
/*
   Plugin Name: Audima
   Plugin URI: http://audima.co/
   Version: 0.5.1
   Author: Audima
   Description: Insere o Player da Audima nos seus Posts
   Text Domain: audima
   Author: Audima
   Author URI: http://audima.co/
   License: Proprietary
  */
?>
<?php
 $Audima_minimalRequiredPhpVersion = '5.0'; function Audima_noticePhpVersionWrong() { global $Audima_minimalRequiredPhpVersion; echo '<div class="updated fade">' . __('Error: plugin "Audima" requires a newer version of PHP to be running.', 'audima') . '<br/>' . __('Minimal version of PHP required: ', 'audima') . '<strong>' . $Audima_minimalRequiredPhpVersion . '</strong>' . '<br/>' . __('Your server\'s PHP version: ', 'audima') . '<strong>' . phpversion() . '</strong>' . '</div>'; } function Audima_PhpVersionCheck() { global $Audima_minimalRequiredPhpVersion; if (version_compare(phpversion(), $Audima_minimalRequiredPhpVersion) < 0) { add_action('admin_notices', 'Audima_noticePhpVersionWrong'); return false; } return true; } function Audima_i18n_init() { $pluginDir = dirname(plugin_basename(__FILE__)); load_plugin_textdomain('audima', false, $pluginDir . '/languages/'); } add_action('plugins_loadedi', 'Audima_i18n_init'); if (Audima_PhpVersionCheck()) { include_once('audima_init.php'); Audima_init(__FILE__); } 